package ru.developer.alexangan.theguardianmobileexplorer.ViewOverrides;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import ru.developer.alexangan.theguardianmobileexplorer.Interfaces.ScrollViewListener;

public class ScrollViewEx extends ScrollView {
    private ScrollViewListener scrollViewListener = null;
    public ScrollViewEx(Context context) {
        super(context);
    }

    public ScrollViewEx(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ScrollViewEx(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setScrollViewListener(ScrollViewListener scrollViewListener) {
        this.scrollViewListener = scrollViewListener;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (scrollViewListener != null) {
            scrollViewListener.onScrollChanged(this, l, t, oldl, oldt);
        }
    }
}

