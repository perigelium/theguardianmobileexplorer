package ru.developer.alexangan.theguardianmobileexplorer.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import ru.developer.alexangan.theguardianmobileexplorer.API.RetrofitAPI;
import ru.developer.alexangan.theguardianmobileexplorer.Entities.ContentFields;
import ru.developer.alexangan.theguardianmobileexplorer.Entities.ContentUnit;
import ru.developer.alexangan.theguardianmobileexplorer.EventHandlers.OnSwipeTouchListener;
import ru.developer.alexangan.theguardianmobileexplorer.Interfaces.NewsCommunicator;
import ru.developer.alexangan.theguardianmobileexplorer.R;
import ru.developer.alexangan.theguardianmobileexplorer.Utils.NetworkUtils;
import ru.developer.alexangan.theguardianmobileexplorer.Utils.ViewUtils;

import static ru.developer.alexangan.theguardianmobileexplorer.Entities.GlobalConstants.API_KEY;


public class FragNewsDetailed extends Fragment implements View.OnClickListener, Callback<ContentUnit>
{

    private NewsCommunicator mCommunicator;
    Activity activity;
    private String apiContentUrl;
    private ProgressDialog requestServerDialog;
    private ImageView ivNewsImage;
    private WebView wvNewsBody;
    private TextView tvNewsTitle;
    private int curListPosition;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        activity = getActivity();
        mCommunicator = (NewsCommunicator) getActivity();

        requestServerDialog = new ProgressDialog(getActivity(), R.style.AppTheme);
        requestServerDialog.setTitle("");
        requestServerDialog.setMessage(getString(R.string.DownloadingDataPleaseWait));
        requestServerDialog.setIndeterminate(true);

        if (getArguments() != null)
        {
            apiContentUrl = getArguments().getString("api_content_url");
            curListPosition = getArguments().getInt("cur_list_item");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.frag_news_detailed_layout, container, false);

        LinearLayout llReturn = (LinearLayout) rootView.findViewById(R.id.llReturn);
        llReturn.setOnClickListener(this);

        OnSwipeTouchListener onSwipeTouchListener = new OnSwipeTouchListener(activity)
        {
            @Override
            public void onSwipeLeft()
            {
                mCommunicator.onPrevNextNews(curListPosition + 1);
            }

            @Override
            public void onSwipeRight()
            {
                mCommunicator.onPrevNextNews(curListPosition - 1);
            }
        };

        LinearLayout llNewsDetailedView = (LinearLayout) rootView.findViewById(R.id.llNewsDetailedView);


        tvNewsTitle = (TextView) rootView.findViewById(R.id.tvNewsCategory);
        ivNewsImage = (ImageView) rootView.findViewById(R.id.ivNewsImage);
        wvNewsBody = (WebView) rootView.findViewById(R.id.wvNewsBody);

        wvNewsBody.setOnTouchListener(onSwipeTouchListener);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        if (NetworkUtils.isNetworkAvailable(activity))
        {
            requestServerDialog.show();

                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        RetrofitAPI retrofitService = RetrofitAPI.retrofit.create(RetrofitAPI.class);

                        retrofit2.Call <ContentUnit> contentCall =
                                retrofitService.getNewsContent(apiContentUrl, API_KEY, "headline,thumbnail,body");

                        contentCall.enqueue(FragNewsDetailed.this);
                    }
                }, 100);
        }
    }

    private void showContentView(ContentFields contentFields)
    {
        tvNewsTitle.setText(contentFields.headline);

        Picasso.with(activity)
                .load(contentFields.thumbnail)
/*                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder_error)*/
                .into(ivNewsImage);

        wvNewsBody.loadData(contentFields.body, "text/html", "UTF-8");
    }

    @Override
    public View getView()
    {
        return super.getView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.llReturn)
        {
            mCommunicator.onDetailedNewsReturned();
            return;
        }
    }

    @Override
    public void onResponse(retrofit2.Call<ContentUnit> call, retrofit2.Response<ContentUnit> response)
    {
        requestServerDialog.dismiss();

        if (response.isSuccessful())
        {
            ContentUnit contentUnit = response.body();

            if (contentUnit != null && contentUnit.contentPackage !=null && contentUnit.contentPackage.contentItem != null)
            {
                ContentFields contentFields = contentUnit.contentPackage.contentItem.contentFields;

                if (this.isVisible())
                {
                    showContentView(contentFields);
                }
            }
        } else
        {
            ViewUtils.showToastMessage(activity, getString(R.string.ServerError));

            //int statusCode = response.code();
            ResponseBody errorBody = response.errorBody();

            try
            {
                Log.d("DEBUG", errorBody.string());

            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(retrofit2.Call<ContentUnit> call, Throwable t)
    {
        ViewUtils.showToastMessage(activity, getString(R.string.ServerAnswerNotReceived));

        requestServerDialog.dismiss();
    }
}
