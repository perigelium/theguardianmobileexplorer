package ru.developer.alexangan.theguardianmobileexplorer.API;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;
import ru.developer.alexangan.theguardianmobileexplorer.Entities.ContentUnit;
import ru.developer.alexangan.theguardianmobileexplorer.Entities.NewsUnit;

import static ru.developer.alexangan.theguardianmobileexplorer.Entities.GlobalConstants.API_HOST_URL;

public interface RetrofitAPI
{
    @GET("search")
    Call<NewsUnit> getNewsPackage(@Query("api-key") String apiKey, @Query("q") String searchStr, @Query("page") int startPage,
                                  @Query("page-size") int pages, @Query("show-fields") String fieldsMode);

    @GET()
    Call<ContentUnit> getNewsContent(@Url String url, @Query("api-key") String apiKey, @Query("show-fields") String fieldsMode);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(API_HOST_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
