package ru.developer.alexangan.theguardianmobileexplorer.Entities;


import com.google.gson.annotations.SerializedName;

public class NewsItem {

    public String id;
    public String type;
    public String sectionId;
    public String sectionName;
    public String webPublicationDate;
    public String webTitle;
    public String webUrl;
    public String apiUrl;
    public Boolean isHosted;
    public String pillarId;
    public String pillarName;
    @SerializedName("fields")
    public ContentFields contentFields;

}
