package ru.developer.alexangan.theguardianmobileexplorer.Entities;

import com.google.gson.annotations.SerializedName;

public class ContentPackage
{

    public String status;
    public String userTier;
    public Long total;
    @SerializedName("content")
    public ContentItem contentItem;

}
