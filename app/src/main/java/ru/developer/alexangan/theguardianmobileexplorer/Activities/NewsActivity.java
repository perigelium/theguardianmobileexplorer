package ru.developer.alexangan.theguardianmobileexplorer.Activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import ru.developer.alexangan.theguardianmobileexplorer.Fragments.FragNewsDetailed;
import ru.developer.alexangan.theguardianmobileexplorer.Fragments.FragNewsList;
import ru.developer.alexangan.theguardianmobileexplorer.Interfaces.NewsCommunicator;
import ru.developer.alexangan.theguardianmobileexplorer.R;
import ru.developer.alexangan.theguardianmobileexplorer.Utils.ViewUtils;

import static ru.developer.alexangan.theguardianmobileexplorer.Entities.GlobalConstants.APP_LOG_TAG;


public class NewsActivity extends Activity implements NewsCommunicator, View.OnClickListener
{
    private FragmentManager mFragmentManager;
    private FragNewsList fragNewsList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_layout);

        fragNewsList = new FragNewsList();
        replaceFragment(FragNewsList.class.getSimpleName(), fragNewsList);
    }

    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.llReturn)
        {
            this.finish();
        }
    }

    @Override
    public void onBackPressed()
    {
        if(mFragmentManager.getBackStackEntryCount() == 1)
        {
            this.finish();
        }
        else
        {
            mFragmentManager.popBackStack();
        }
    }

    @Override
    public void onNewsListItemSelected(int position)
    {
            if(fragNewsList.l_newsItems != null && fragNewsList.l_newsItems.size() != 0)
            {
                removeFragment(FragNewsDetailed.class.getSimpleName());

                FragNewsDetailed fragNewsDetailed = new FragNewsDetailed();

                Bundle args = new Bundle();
                args.putString("api_content_url", fragNewsList.l_newsItems.get(position).apiUrl);
                args.putInt("cur_list_item", position);
                fragNewsDetailed.setArguments(args);

                replaceFragment(FragNewsDetailed.class.getSimpleName(), fragNewsDetailed);
            }
    }

    @Override
    public void onDetailedNewsReturned()
    {
        if (!fragNewsList.isAdded())
        {
            mFragmentManager.popBackStack();
        }
    }

    @Override
    public void onPrevNextNews(int position)
    {
        if(position >= fragNewsList.l_newsItems.size() || position < 0)
        {
            ViewUtils.showToastMessage(this, getString(R.string.NoSuchArticle));
            return;
        }

        onNewsListItemSelected(position);
    }

    @Override
    public void onClose()
    {
        this.finish();
    }

    public void replaceFragment(String fragmentTag, Fragment newFragment)
    {
        mFragmentManager = getFragmentManager();
        Fragment fragReplacement = mFragmentManager.findFragmentByTag(fragmentTag);

        if (fragReplacement == null)
        {
            fragReplacement = newFragment;
        }

        if (!fragReplacement.isAdded())
        {
            FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();

            mFragmentTransaction.replace(R.id.newsFragContainer, fragReplacement, fragmentTag);

            try
            {
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commitAllowingStateLoss();
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private void removeFragment(String fragmentTag)
    {
        mFragmentManager = getFragmentManager();
        Fragment fragment = mFragmentManager.findFragmentByTag(fragmentTag);

        if (fragment != null)
        {
            Log.i(APP_LOG_TAG, ": - removing fragment " + fragment.getTag());
            FragmentTransaction pmFragmentTransaction = mFragmentManager.beginTransaction();
            pmFragmentTransaction.detach(fragment);
            pmFragmentTransaction.remove(fragment);

            try
            {
                pmFragmentTransaction.commitAllowingStateLoss();
                mFragmentManager.popBackStack();
                mFragmentManager.executePendingTransactions();
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
