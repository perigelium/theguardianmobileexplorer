package ru.developer.alexangan.theguardianmobileexplorer.Fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.developer.alexangan.theguardianmobileexplorer.API.RetrofitAPI;
import ru.developer.alexangan.theguardianmobileexplorer.Adapters.NewsListAdapter;
import ru.developer.alexangan.theguardianmobileexplorer.Entities.NewsItem;
import ru.developer.alexangan.theguardianmobileexplorer.Entities.NewsUnit;
import ru.developer.alexangan.theguardianmobileexplorer.Interfaces.NewsCommunicator;
import ru.developer.alexangan.theguardianmobileexplorer.Interfaces.ScrollViewListener;
import ru.developer.alexangan.theguardianmobileexplorer.R;
import ru.developer.alexangan.theguardianmobileexplorer.Utils.NetworkUtils;
import ru.developer.alexangan.theguardianmobileexplorer.Utils.ViewUtils;
import ru.developer.alexangan.theguardianmobileexplorer.ViewOverrides.ScrollViewEx;

import static ru.developer.alexangan.theguardianmobileexplorer.Entities.GlobalConstants.API_KEY;
import static ru.developer.alexangan.theguardianmobileexplorer.Entities.GlobalConstants.APP_PREFERENCES;
import static ru.developer.alexangan.theguardianmobileexplorer.Entities.GlobalConstants.NEWS_PER_PAGE;
import static ru.developer.alexangan.theguardianmobileexplorer.Entities.GlobalConstants.STR_INITIAL_QUERY;

public class FragNewsList extends ListFragment implements View.OnClickListener, ScrollViewListener, SearchView.OnQueryTextListener, Callback<NewsUnit>
{
    private NewsCommunicator mCommunicator;
    private Activity activity;
    private Call callGetNewsList;
    private ProgressDialog requestServerDialog;

    private SearchView svNews;
    private TextView tvCloseSearchView;
    private int firstNewsNumberInList;
    private boolean firstStart;
    private ImageView closeIcon;
    private ImageView magImage;
    private TextView textView;
    private LinearLayout llOpenSearchView;
    public ArrayList<NewsItem> l_newsItems;
    private String strLastQuery;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        activity = getActivity();
        mCommunicator = (NewsCommunicator) getActivity();

        requestServerDialog = new ProgressDialog(getActivity(), R.style.AppTheme);
        requestServerDialog.setTitle("");
        requestServerDialog.setMessage(getString(R.string.DownloadingDataPleaseWait));
        requestServerDialog.setIndeterminate(true);

        firstStart = true;
        l_newsItems = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.frag_news_layout, container, false);

        LinearLayout llReturn = (LinearLayout) rootView.findViewById(R.id.llReturn);
        llReturn.setOnClickListener(this);

        svNews = (SearchView) rootView.findViewById(R.id.svNews);

        llOpenSearchView = (LinearLayout) rootView.findViewById(R.id.llOpenSearchView);

        tvCloseSearchView = (TextView) rootView.findViewById(R.id.tvCloseSearchView);

        ScrollViewEx scvNews = (ScrollViewEx) rootView.findViewById(R.id.scvNews);
        scvNews.setScrollViewListener(this);

        int searchHintBtnId = svNews.getContext().getResources().getIdentifier("android:id/search_mag_icon", null, null);
        int hintTextId = svNews.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        int closeBtnId = svNews.getContext().getResources().getIdentifier("android:id/search_close_btn", null, null);

        closeIcon = (ImageView) svNews.findViewById(closeBtnId);
        textView = (TextView) svNews.findViewById(hintTextId);

        magImage = (ImageView) svNews.findViewById(searchHintBtnId);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        textView.setTextColor(Color.BLACK);
        textView.setHintTextColor(Color.parseColor("#ff808080"));

        closeIcon.setImageResource(R.drawable.clearsearch_2);
        magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));

        strLastQuery = STR_INITIAL_QUERY;
        firstNewsNumberInList = 1;

        llOpenSearchView.setOnClickListener(this);
        tvCloseSearchView.setOnClickListener(this);

        svNews.setSubmitButtonEnabled(true);
        svNews.setOnQueryTextListener(this);

        svNews.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(final View v, boolean hasFocus)
            {
                if (!hasFocus)
                {
                    closeSearchView(tvCloseSearchView);
                }
            }
        });

        if (firstStart && l_newsItems.size() == 0)
        {
            firstStart = false;
            addNewsToList(strLastQuery);

        } else if (l_newsItems != null && l_newsItems.size() != 0)
        {
            updateNewsListView(l_newsItems);
        }
    }

    @Override
    public View getView()
    {
        return super.getView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.llReturn)
        {
            mCommunicator.onClose();
            return;
        }

        if (view.getId() == R.id.llOpenSearchView)
        {
            view.setVisibility(View.GONE);
            svNews.setVisibility(View.VISIBLE);
            tvCloseSearchView.setVisibility(View.VISIBLE);
            return;
        }

        if (view.getId() == R.id.tvCloseSearchView)
        {
            closeSearchView(view);
        }
    }

    private void closeSearchView(View view)
    {
        view.setVisibility(View.GONE);
        svNews.setVisibility(View.GONE);
        llOpenSearchView.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onQueryTextSubmit(String queryString)
    {
        closeSearchView(tvCloseSearchView);
        ViewUtils.disableSoftKeyboard(activity);

        SharedPreferences mSettings = activity.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        mSettings.edit().putString("newsSearchLastQueryString", queryString).apply();

        String strQueryLower = queryString.toLowerCase();
        strLastQuery = strQueryLower;

        firstNewsNumberInList = 1;
        l_newsItems.clear();
        updateNewsListView(l_newsItems);

        addNewsToList(strLastQuery);

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText)
    {
        return false;
    }

    @Override
    public void onFailure(Call<NewsUnit> call, Throwable t)
    {
        ViewUtils.showToastMessage(activity, getString(R.string.ServerAnswerNotReceived));

        requestServerDialog.dismiss();

        if (l_newsItems.size() == 0)
        {
            mCommunicator.onClose();
        }
    }

    @Override
    public void onResponse(Call<NewsUnit> callGetNewsList, Response<NewsUnit> response)
    {
        requestServerDialog.dismiss();

        if (response.isSuccessful())
        {
            NewsUnit newsUnit = response.body();

            if (newsUnit != null && newsUnit.newsPackage.newsItems != null && newsUnit.newsPackage.newsItems.size() != 0)
            {
                l_newsItems.addAll(newsUnit.newsPackage.newsItems);

                if (this.isVisible())
                {
                    updateNewsListView(l_newsItems);
                }

                firstNewsNumberInList = l_newsItems.size() + 1;
            }
        } else
        {
            ViewUtils.showToastMessage(activity, getString(R.string.ServerError));

            //int statusCode = response.code();
            ResponseBody errorBody = response.errorBody();

            try
            {
                Log.d("DEBUG", errorBody.string());

            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void updateNewsListView(ArrayList<NewsItem> l_newsItems)
    {
        NewsListAdapter newsListAdapter = new NewsListAdapter(getActivity(), R.layout.news_row, l_newsItems);
        setListAdapter(newsListAdapter);

        ListView lv = getListView();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                mCommunicator.onNewsListItemSelected(position);
            }
        });
    }

    @Override
    public void onScrollChanged(ScrollViewEx scrollView, int x, int y, int oldx, int oldy)
    {
        View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
        int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

        if (diff == 0)
        {
            addNewsToList(strLastQuery);
        }
    }

    private void addNewsToList(final String strQuery)
    {
        if (!NetworkUtils.isNetworkAvailable(activity))
        {
            ViewUtils.showToastMessage(activity, getString(R.string.OfflineMode));
            return;
        }

        requestServerDialog.show();

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                RetrofitAPI retrofitService = RetrofitAPI.retrofit.create(RetrofitAPI.class);

                callGetNewsList = retrofitService.getNewsPackage(API_KEY, strQuery, firstNewsNumberInList, NEWS_PER_PAGE, "thumbnail");

                callGetNewsList.enqueue(FragNewsList.this);
            }
        }, 100);
    }
}
