package ru.developer.alexangan.theguardianmobileexplorer.Entities;

import com.google.gson.annotations.SerializedName;

public class ContentItem
{

    public String id;
    public String type;
    public String sectionId;
    public String sectionName;
    public String webPublicationDate;
    public String webTitle;
    public String webUrl;
    public String apiUrl;
    @SerializedName("fields")
    public ContentFields contentFields;
    public Boolean isHosted;
    public String pillarId;
    public String pillarName;

}
