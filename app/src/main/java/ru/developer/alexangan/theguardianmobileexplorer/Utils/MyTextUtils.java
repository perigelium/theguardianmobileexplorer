package ru.developer.alexangan.theguardianmobileexplorer.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MyTextUtils
{
    public static String reformatDateString(String datePost)
    {
        SimpleDateFormat sdfFull = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
        Date dateNews;

        try
        {
            dateNews = sdfFull.parse(datePost);
            sdfFull = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH); // "E MMMM d, yyyy"
            String formattedDate = sdfFull.format(dateNews);

            return formattedDate;

        } catch (ParseException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
