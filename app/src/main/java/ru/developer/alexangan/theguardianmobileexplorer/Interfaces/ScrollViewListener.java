package ru.developer.alexangan.theguardianmobileexplorer.Interfaces;


import ru.developer.alexangan.theguardianmobileexplorer.ViewOverrides.ScrollViewEx;

public interface ScrollViewListener
{
    void onScrollChanged(ScrollViewEx scrollView,
                         int x, int y, int oldx, int oldy);
}
