package ru.developer.alexangan.theguardianmobileexplorer.Entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsPackage {

    public String status;
    public String userTier;
    public Long total;
    public Long startIndex;
    public Long pageSize;
    public Long currentPage;
    public Long pages;
    public String orderBy;
    @SerializedName("results")
    public List<NewsItem> newsItems = null;

}
