package ru.developer.alexangan.theguardianmobileexplorer.Entities;

public class GlobalConstants
{
    public static final String APP_LOG_TAG = "DEBUG";
    public static final String APP_PREFERENCES = "mysettings";

    public static final String API_HOST_URL = "https://content.guardianapis.com/";
    public static final String API_KEY = "59a7414c-d96b-4329-8704-2f975e5630e5";
    public static final Integer NEWS_PER_PAGE = 7;
    public static final String STR_INITIAL_QUERY = "news";
}


