package ru.developer.alexangan.theguardianmobileexplorer.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ru.developer.alexangan.theguardianmobileexplorer.Entities.NewsItem;
import ru.developer.alexangan.theguardianmobileexplorer.R;
import ru.developer.alexangan.theguardianmobileexplorer.Utils.MyTextUtils;

public class NewsListAdapter extends BaseAdapter
{
    private Context mContext;
    private ArrayList<NewsItem> lNewsItems;
    private int layout_id;

    public NewsListAdapter(Context context, int layout_id, ArrayList<NewsItem> lNewsItems)
    {
        mContext = context;
        this.lNewsItems = lNewsItems;
        this.layout_id = layout_id;
    }

    @Override
    public int getCount()
    {
        return lNewsItems.size();
    }

    @Override
    public Object getItem(int i)
    {
        return i;
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(layout_id, parent, false);

        TextView tvNewsCategory = (TextView) row.findViewById(R.id.tvNewsCategory);
        TextView tvNewsTitle = (TextView) row.findViewById(R.id.tvNewsTitle);
        TextView tvNewsDate = (TextView) row.findViewById(R.id.tvNewsDate);
        ImageView ivNewsThumb = (ImageView) row.findViewById(R.id.ivNewsThumb);

        String strNewsSectionName = lNewsItems.get(position).sectionName;
        String strNewsSubtitle = lNewsItems.get(position).webTitle;
        String strPublicationDate = lNewsItems.get(position).webPublicationDate;

        String formattedDate = MyTextUtils.reformatDateString(strPublicationDate);
        tvNewsDate.setText(formattedDate);

        String strNewsThumb = lNewsItems.get(position).contentFields != null ? lNewsItems.get(position).contentFields.thumbnail : null;

            tvNewsCategory.setText(strNewsSectionName);

            tvNewsTitle.setText(strNewsSubtitle);

        if (strNewsThumb != null)
        {
            Picasso.with(mContext).load(strNewsThumb)
/*                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder_error)*/
                .into(ivNewsThumb);
        }

        return row;
    }
}
