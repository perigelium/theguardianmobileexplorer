package ru.developer.alexangan.theguardianmobileexplorer.Interfaces;


public interface NewsCommunicator
{
    void onNewsListItemSelected(int position);

    void onDetailedNewsReturned();

    void onPrevNextNews(int direction);

    void onClose();
}